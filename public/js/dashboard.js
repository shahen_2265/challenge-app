$(() => {
	const START_QUIZ = "start_quiz";
	const FAILED_START_QUIZE = 'failed_to_start_quize';
	const OPPONENT_LEAVE_QUIZE = 'opponend_leave_quize';

	const signedToken = Cookie.getToken();

	if (!signedToken) {
		throw new Error('Unauthorized');
		window.location.href = '/signin';
	}
	const token = signedToken.split('Barear ')[1];

	const socket = io.connect('', {
		query: `token=${token}`
	});

	socket.on('connect', () => {
		console.log('connected');
	});
	socket.on(FAILED_START_QUIZE, (data) => {
		console.log(data);
	});
	socket.on(OPPONENT_LEAVE_QUIZE, (data) => {
		console.log('Leave', data);
	});
	socket.on('authenticated', (e) => console.log(e));
	socket.on('disconnect', () => {
		console.log('on disconnect');
	});
	socket.on(START_QUIZ, (data) => {
		console.log(data);
		startQuiz();
	});

	$('.start-game-btn').on('click', function() {
		const id = $(this).attr('data-id');
		socket.emit(START_QUIZ, { id });
	});

	function startQuiz() {

	}
});