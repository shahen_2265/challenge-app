$(() => {
	const submitButton = $('#submit');
	const username = $('#name');
	const password = $('#paw');

	submitButton.on('click', handleSubmit);

	/**
	 * Handle submit button press
	*/
	function handleSubmit() {
		const credentials = {
			username: username.val(),
			password: password.val()
		};
		Api
			.authenticate(credentials)
			.then((res) => res.json())
			.then((res) => {
				window.location.href = '/';
			})
			.catch((e) => {
				console.log('error', e);
			});
	}
});