(() => {
	function Cookie() {
		this.get = get;
		this.getToken = getToken;
		this.removeToken = removeToken;

		function get(name) {
			const regexp = "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)";

			const matches = document.cookie.match(new RegExp(regexp))
			return matches
				? decodeURIComponent(matches[1])
				: undefined;
		}

		function getToken() {
			return get('Authorization');
		}

		function removeToken() {
			const cookie = document.cookie;
			document.cookie.split(
				cookie.substr(cookie.indexOf('Authorization'), cookie.indexOf(';', cookie.indexOf('Authorization')))
			).join(' ');


		}
	}

	window.Cookie = new Cookie();
})();