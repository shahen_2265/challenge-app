(() => {
	function Api() {
		this.authenticate = authenticate;
		this.getUsers = getUsers;
		this.getQuizes = getQuizes;
	
		function authenticate(credentials) {
			return fetch('/api/v1/auth/signin', {
				method: 'POST',
				body: JSON.stringify(credentials),
				headers: new Headers({
					'Content-Type': 'application/json',
				}),
				'credentials': 'same-origin'
			});
		}

		function getUsers() {
			const token = localStorage.getItem('challenge-token');
			return fetch('/api/v1/users', {
				headers: new Headers({
					Authorization: token
				})
			});
		}

		function getQuizes() {
			// TODO
		}
	}
	window.Api = new Api();
})();