$(() => {
	$('#logoutbtn').on('click', () => {
		fetch('/api/v1/auth/signout')
			.then(() => window.location.reload())
			.catch(() => alert('Something went wrong'));
	});
});