/**
 * @flow
 */
export default async function(ctx, next) {
	ctx.setToken = (token: string) => {
		const now = new Date();
		const year = 1000 * 60 * 60 * 24 * 365;
		const expires = new Date(now.getTime() + year);

		ctx.cookies.set(
			process.env.JWT_COOKIE,
			token, {
				httpOnly: false,
				secure: false,
				expires,
				overwrite: true,
			}
		);
	}
	ctx.getToken = () => ctx.cookies.get(process.env.JWT_COOKIE);
	ctx.removToken = () => ctx.cookies.set(process.env.JWT_COOKIE, null, {
		httpOnly: false,
		secure: false,
		overwrite: true,
	});

	await next();
}