import { isTokenValid } from '../utils/jwt-auth';

class Redirects {
	authorizedRequests(path: string) {
		return async function(ctx, next: Function) {
			const token = ctx.getToken();
			if (isTokenValid(token)) {
				ctx.redirect(path);
				return;
			}
			return next();
		}
	}

	unauthorizedRequests(path: string) {
		return async function(ctx, next: Function) {
			const token = ctx.getToken();

			if (isTokenValid(token)) {
				return next();
			}
			ctx.redirect(path);
		}
	}
}

export default new Redirects();