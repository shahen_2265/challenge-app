import mongoose from 'mongoose';
import { capitalize } from 'lodash';

// ctx.models.user
export default async function(ctx, next) {
	ctx.models = new Proxy(mongoose, {
		get: (target, name) => {
			return mongoose.model(capitalize(name));
		}
	});

	await next();
}