/**
 * @flow
 */
import mongoose, { Schema } from 'mongoose';
import joi from 'joi';
import { validatePromise } from '../../utils/validatePromise';

const quizesSchema = new Schema({
	questions: [{
		description: {
			type: String,
			required: true
		},
		answers: [{
			text: { type: String, required: true },
			isCorrect: Boolean
		}]
	}]
});

quizesSchema.statics.getAcceptableFields = () => [
	'questions'
];

quizesSchema.statics.isValid = (draftQuiz: {}): Promise<{ error: string }> => {
	const schema = joi.object().keys({
		questions: joi.array().items({
			description: joi.string().min(4).max(50).required(),
			answers: joi.array().items({
				text: joi.string().required(),
				isCorrect: joi.bool().required()
			}).min(1).required()
		}).min(1).required()
	});
	return validatePromise(schema, draftQuiz);
};

mongoose.model('Quiz', quizesSchema);