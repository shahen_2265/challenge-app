/**
 * @flow
 */

import { pick } from 'lodash';

export async function createQuize(ctx) {
	const quizModel = ctx.models.quiz;
	const { body } = ctx.request;
	const draftQuiz = pick(body, quizModel.getAcceptableFields());
	const { error } = await quizModel.isValid(body);
	if (error) {
		ctx.throw(400, error);
	}
	try {
		const quiz = await quizModel.create(body);
		ctx.body = quiz;
	} catch (e) {
		ctx.throw(422, e);
	}
}
export async function getQuizes(ctx) {
	const quizesModel = ctx.models.quiz;

	const quizes = await quizesModel.find({}, '-questions.answers.isCorrect')

	ctx.body = quizes;
}