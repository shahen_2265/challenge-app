/**
 * @flow
 */

import Router from 'koa-router';
import Boom from 'boom';
import debug from 'debug';
import { authRequired } from '../../utils/jwt-auth';
import { createQuize, getQuizes } from './quizes.handles';

const log = debug('challenge:quizes.routes');

export function attach(app, prefix: string) {
	const route = `${prefix}/quizes`;
	const router = new Router({
		prefix: route
	});
	// router.use(authRequired);

	router.post('/', createQuize);
	router.get('/', getQuizes);

	app.use(router.routes());
	app.use(router.allowedMethods({
		notImplemented: () => new Boom.notImplemented(),
		methodNotAllowed: () => new Boom.methodNotAllowed()
	}));

	log('Quizes routes initialized', route);
}