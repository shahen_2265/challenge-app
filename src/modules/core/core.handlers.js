/**
 * @flow
 */

export async function renderIndex(ctx) {
	const user = ctx.state.user;
	const users = await ctx.cache.getAllUsers();
	return ctx.render('dashboard', { user, users });
}

export async function renderSignin(ctx) {
	return ctx.render('signin');
}

export async function renderSignup(ctx) {
	return ctx.render('signup');
}