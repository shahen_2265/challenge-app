/**
 * @flow
 */
import Router from 'koa-router';
import debug from 'debug';
import Boom from 'boom';
import {
	renderIndex,
	renderSignin,
	renderSignup
} from './core.handlers';
import { redirects } from '../../middlewares/index';

const log = debug('challenge:core');

const SIGNIN_ROUTE = '/signin';
const SIGNUP_ROUTE = '/signup';

export function attach(app, _) {
	log('Initialize core routes');

	const router = new Router({});
	router.get('/', 
		redirects.unauthorizedRequests('/signin'),
		renderIndex
	);

	router.use([SIGNIN_ROUTE, SIGNUP_ROUTE], redirects.authorizedRequests('/'));
	router.get(SIGNIN_ROUTE, renderSignin);
	router.get(SIGNUP_ROUTE, renderSignup);

	app.use(router.routes());
	app.use(router.allowedMethods({
		notImplemented: () => new Boom.notImplemented(),
		methodNotAllowed: () => new Boom.methodNotAllowed()
	}));
}