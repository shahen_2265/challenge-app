/**
 * @flow
 */
import Router from 'koa-router';
import Boom from 'boom';
import debug from 'debug';
import { signup, signin, signout } from './auth.handlers';
import { authRequired } from '../../utils/jwt-auth';

const log = debug('challenge:auth.routes');

export function attach(app, prefix: string) {
	const route = `${prefix}/auth`;
	const router = new Router({
		prefix: route
	});
	router.post('/signup', signup);
	router.post('/signin', signin);
	router.get('/signout', signout);

	app.use(router.routes());
	app.use(router.allowedMethods({
		notImplemented: () => new Boom.notImplemented(),
		methodNotAllowed: () => new Boom.methodNotAllowed()
	}));

	log('Auth routes initialized', route);
}