import mongoose from 'mongoose';
import { pick } from 'lodash';
import jwt from 'jsonwebtoken';

const generateToken = (ctx, user: {}): string => {
	const token = jwt.sign({ user }, process.env.JWT_SECRET, {
		expiresIn: '100d'
	});
	const signature = process.env.TOKEN_SIGNATURE;

	return [
		signature,
		token
	].join(' ');
}

export async function signup(ctx) {
	const { body } = ctx.request;
	const usersModel = ctx.models.user;
	const draftUser = pick(body, usersModel.getAcceptableFields());

	const { error } = await usersModel.isValid(draftUser);

	if (error) {
		ctx.throw(400, error);
	}

	try {
		const user = await usersModel.create(draftUser);
		ctx.body = user.toJSON();
		const cookieUser = pick(ctx.body, ['_id', 'username', 'name']);
		ctx.setToken(generateToken(ctx, cookieUser));
	} catch (e) {
		ctx.throw(400, e);
	}
}

export async function signin(ctx) {
	const { body } = ctx.request;
	const usersModel = ctx.models.user;
	const { error } = await usersModel.validateAuthentication(body);

	if (error) {
		ctx.throw(400, error);
	}

	const possibleUser = await usersModel.findOne({ username: body.username });

	if (!possibleUser) {
		ctx.throw(401);
	}

	if (!possibleUser.comparePassword(body.password)) {
		ctx.throw(401);
	}

	ctx.body = possibleUser;
	const cookieUser = pick(possibleUser, ['_id', 'username', 'name']);
	const token = generateToken(ctx, cookieUser);
	ctx.setToken(token);
}

export function signout(ctx) {
	ctx.removToken();
	ctx.status = 200;
}