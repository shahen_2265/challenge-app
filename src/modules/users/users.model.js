/**
 * @flow
 */
import mongoose, { Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import debug from 'debug';
import joi from 'joi';
import { validatePromise } from '../../utils/validatePromise';

type Credentials = {
	username: string,
	password: string
};

const log = debug('challenge:users.model');

log('Initialize Users model');

const userSchema = new Schema({
	name: {
		type: String,
		required: true
	},
	password: {
		type: String,
		required: true
	},
	username: {
		type: String,
		required: true,
		unique: true,
		sparse: true
	}
});
const SALT_WORK_FACTOR = 10;

userSchema.statics.getAcceptableFields = () => [
	'name', 'password', 'username'
];

userSchema.statics.validateAuthentication = (credentials: Credentials): Promise<boolean> => {
	const schema = joi.object().keys({
		username: joi.string().min(3).max(50).required(),
		password: joi.string().min(4).max(50).required()
	}).required();

	return validatePromise(schema, credentials);
}

userSchema.statics.isValid = (draftUser: any): Promise<{ error: any }> => {
	const schema = joi.object().keys({
		name: joi.string().min(3).max(50).required(),
		username: joi.string().min(3).max(50).required(),
		password: joi.string().min(4).max(50).required()
	}).required();
	return validatePromise(schema, draftUser);
}

userSchema.pre('save', function(next: Function) {
	if (!this.isModified('password')) {
		return next();
	}
	bcrypt.genSalt(SALT_WORK_FACTOR, (err, salt) => {
		if (err) {
			return next(err);
		}
		bcrypt.hash(this.password, salt, (err, hash) => {
			if (err) {
				return next(err);
			}
			this.password = hash;
			next();
		});
	});
});

userSchema.methods.comparePassword = function(password: string): Promise<boolean> {
	return new Promise((resolve, reject) => {
		bcrypt
			.compare(password, this.password)
			.then((res) => res ? resolve(res) : reject(res))
			.catch(reject);
	});
}

export default mongoose.model('User', userSchema);

