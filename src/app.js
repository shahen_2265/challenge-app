import debug from 'debug';
import http from 'http';
import connectMongo from './lib/mongoose';
import { attachSocket } from './lib/socket';

const log = debug('challenge:app');

export async function start() {
	log('Starting the app');

	await connectMongo();
	const app = require('./lib/koa').default;
	const server = http.createServer(app.callback());
	attachSocket(app, server);
	server.listen(process.env.PORT);

	log('listening on port ', process.env.PORT);
}