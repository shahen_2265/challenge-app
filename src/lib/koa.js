/**
 * @flow
 */
import Koa from 'koa';
import koaError from 'koa-json-error';
import koaBody from 'koa-body';
import helmet from 'koa-helmet';
import views from 'koa-views';
import koaLogger from 'koa-logger';
import compress from 'koa-compress';
import serve from 'koa-static';
import bouncer from 'koa-bouncer';
import path from 'path';
import debug from 'debug';
import { attachRedis } from './Redis';
import { jwtMethods, attachModels } from '../middlewares';
import { attachRoutes } from './router';
import { parseToken } from '../utils/jwt-auth';

const log = debug('challenge:koa');
const app = new Koa();

app.keys = [process.env.SIGNED_KEYS];

log('initialize middlewares');
const publicFolderPath = path.join(process.cwd(), 'public');
app.use(serve(publicFolderPath));
app.use(views(publicFolderPath, {
	extension: 'pug'
}));
app.use(helmet());
app.use(koaError((err) => ({
	status: err.status,
	message: err.message,
	success: false
})));
app.use(koaBody({ formidable: { uploadDir: __dirname } }));
app.use(bouncer.middleware());
app.use(compress({
	filter: (contentType) => {
		return (/json|text|javascript|css|font|svg/).test(contentType);
	}
}));
app.use(attachModels);
app.use(jwtMethods);
app.use(parseToken);
app.use(koaLogger());

attachRedis(app);
attachRoutes(app);

export default app;