import mongoose from 'mongoose';
import debug from 'debug';
import glob from 'glob';
import path from 'path';

const log = debug('challenge:mongoose');

mongoose.Promise = global.Promise;

const modelsPattern = 'src/modules/**/*.model.js';
const models = path.join(process.cwd(), modelsPattern);

async function initializeModels() {
	log('Initializing models');
	glob.sync(models)
		.forEach((modelPath) => require(modelPath));
	log('Initialzied !!!');
}

export default async function() {
	return mongoose.connect(process.env.MONGO_URI)
		.then(() => log('Connection Established'))
		.then(initializeModels)
		.catch((e) => log('Connection failure', e));
}