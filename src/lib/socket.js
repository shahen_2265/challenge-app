/**
 * @flow
 */
import Socket from 'socket.io/lib/socket';
import IO from 'socket.io';
import socketioJwt from 'socketio-jwt';
import redisAdapter from 'socket.io-redis';
import debug from 'debug';
import { get } from 'lodash';
import isMongoId from '../utils/isMongoId';

const log = debug('challenge:socket');

export const UNAUTHORIZED = 'unauthorized';
export const AUTHENTICATED = 'authenticated';

const CONNECT_EVENT = 'connection';
const DISCONNECT_EVENT = 'disconnect';

const OPPONENT_LEAVE_QUIZE = 'opponend_leave_quize';
const FAILED_START_QUIZE = 'failed_to_start_quize';
const START_QUIZ = 'start_quiz';
const FINISH_QUIZ = 'finish_quiz';
const NEXT_QUESTION = 'next_question';

const adapterOptions = {
	host: process.env.REDIS_HOST,
	port: process.env.REDIS_PORT
};
const jwtOptions = {
	secret: process.env.JWT_SECRET,
	handshake: true,
	decodedPropertyName: 'auth'
};

/**
 * @extension method for socket object
*/
Socket.prototype.getUser = function() {
	return this.auth.user;
};

export function attachSocket(app, server) {
	log('Initialize socket');

	const io = IO(server);
	io.adapter(redisAdapter(adapterOptions));
	io.use(socketioJwt.authorize(jwtOptions));

	io.on(CONNECT_EVENT, (socket) => {
		const user = socket.getUser();

		app.cache.setUser(user._id, {
			...socket.auth.user,
			socketId: socket.id
		});

		socket.on(DISCONNECT_EVENT, () => {
			app.cache.removeUser(user._id)
				.then(() => app.cache.stopQuize(user._id))
				.then((opponent) => {
					console.log(opponent);
					socket.to(opponent.socketId).emit(OPPONENT_LEAVE_QUIZE, user._id);
				})
				.catch(console.log);
		});

		socket.on(START_QUIZ, async (data) => {
			// TODO: Exclude the case when data.id === currentUser.id
			const opponentId = get(data, ['id']);
			if (!isMongoId(opponentId)) {
				log('Invalid user to start quiz', opponentId);
				return;
			}
			const isBusy = await app.cache.isOpponentBusy(opponentId);
			if (isBusy) {
				socket.emit(FAILED_START_QUIZE, { message: 'Opponent is busy' });
				return;
			}
			await app.cache.startQuiz([user._id, opponentId]);
			const opponent = await app.cache.getUser(opponentId);
			socket.emit(START_QUIZ, { opponent });
			socket.to(opponent.socketId).emit(START_QUIZ, {
				opponent: user
			});
		});
	});

	app.context.io = io;
	log('Socket initialized');
	return io;
}