import glob from 'glob';
import path from 'path';
import debug from 'debug';

const log = debug('challenge:router');

const routesPattern = 'src/modules/**/*.routes.js';
const routes = path.join(process.cwd(), routesPattern);
const prefix = `/api/${process.env.API_VERSION}`;

export function attachRoutes(app) {
	log('Initialize Routes');
	glob.sync(routes)
		.forEach((routePath) => {
			require(routePath).attach(app, prefix);
		});
}

