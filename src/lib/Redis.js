/**
 * @flow
 */
import redis from 'redis';
import debug from 'debug';
import { promisify } from 'util';
import { isEmpty } from 'lodash';

const log = debug('challenge:Redis');
const USER_HASH = 'users';
const QUIZ_HASH = 'quizzes';

const parseResponse = (response) => {
	if (!response) {
		return [];
	}
	if (typeof response === 'object') {
		const res = [];
		Object.keys(response)
			.forEach(id => {
				res.push(JSON.parse(response[id]));
			});
		return res;
	}
	if (typeof response === 'string') {
		return JSON.parse(response);
	}
}
class CacheService {
	constructor() {
		this.client = redis.createClient({
			host: process.env.REDIS_HOST,
			port: process.env.REDIS_PORT
		});

		this._get = promisify(this.client.get).bind(this.client);
		this._hget = promisify(this.client.hget).bind(this.client);
		this._hdel = promisify(this.client.hdel).bind(this.client);
		this._hset = promisify(this.client.hset).bind(this.client);
		this._set = promisify(this.client.set).bind(this.client);
		this._remove = promisify(this.client.del).bind(this.client);
		this._hgetall = promisify(this.client.hgetall).bind(this.client);
		this._exists = promisify(this.client.keys).bind(this.client);
	}
	set(key, value) {
		let val = value;
		if (typeof value !== 'string') {
			val = JSON.stringify(value);
		}
		return this._set(key, val);
	}
	get(key: string): Promise<*> {
		return this._get(key)
			.then((res) => {
				try {
					const result = JSON.parse(res);
					return result;
				} catch (e) {
					return res;
				}
			});
	}

	setUser(key: string, value: {}): Promise<string> {
		return this._hset(USER_HASH, key, JSON.stringify(value));
	}

	getUser(key: string): Promise<{}> {
		console.log("GET USER", key);
		return this._hget(USER_HASH, key)
			.then(parseResponse);
	}

	stopQuize(id: string): Promise<{}> {
		return this._exists(`*${id}*`)
			.then(res => {
				const usersToNotify = [];
				if (!Array.isArray(res)) {
					return Promise.resolve();
				}
				if (isEmpty(res)) {
					return Promise.resolve();
				}
				let opponentId = null;
				const promises = [];

				res.forEach((_id) => {
					opponentId = _id
						.replace(new RegExp(id), '')
						.replace('_', '');

					promises.push(this._remove(_id));
				});
				console.log(opponentId);
				return Promise.all(promises)
					.then(() => this.getUser(opponentId));
			});
	}

	async isOpponentBusy(id): Promise<[]> {
		return this._exists(`*${id}*`)
			.then((res) => res.length > 0);
	}

	startQuiz(opponents: string[]): Promise<string> {
		return this._set(opponents.join("_"), JSON.stringify({ createdAt: Date.now() }));
	}

	getAllUsers(): Promise<[{}]> {
		return this._hgetall(USER_HASH)
			.then(parseResponse);
	}

	remove(key: string): Promise<*> {
		return this._remove(key);
	}

	removeUser(key: string): Promise<string> {
		return this._hdel(USER_HASH, key);
	}
}

const cache = new CacheService();
export default cache;

export function attachRedis(app) {
	log('Initialize Redis Cache service');
	app.cache = cache;
	app.context.cache = cache;
}