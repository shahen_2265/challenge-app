import jwt from 'jsonwebtoken';
import { isEmpty, isNil } from 'lodash';

const cookieName = process.env.JWT_COOKIE;
const signature = process.env.TOKEN_SIGNATURE;
const secret = process.env.JWT_SECRET;

export function isTokenValid(token: string): {} | boolean {
	try {
		const signedToken = token.split(signature + ' ')[1];
		const { user } = jwt.verify(signedToken, secret);
		return user;
	} catch (e) {
		return false
	}
}

export async function parseToken(ctx, next) {
	const token = ctx.getToken();
	if (!token) {
		return next();
	}
	try {
		const signedToken = token.split(signature + ' ')[1];
		const { user } = jwt.verify(signedToken, secret);
		ctx.state.user = user;
	} catch (e) { }
	return next();
}

export async function authRequired(ctx, next) {
	const token = ctx.getToken();
	if (isNil(token)) {
		ctx.throw(401);
		return;
	}
	try {
		
		const signedToken = token.split(signature + ' ')[1];
		const { user } = jwt.verify(signedToken, secret);
		ctx.state.user = user;
	} catch(e) {
		return ctx.throw(401);
	}
	if (!isNil(next)) {
		await next();
	}
}

export function generateToken(ctx, user) {
	const token = jwt.sign({ user }, process.env.JWT_SECRET, {
		expiresIn: '100d'
	});
	const signedToken = [signature, token].join(' ');
	
	ctx.setToken(signedToken);
}
