const isHexdecimal = (id) => /^[0-9A-F]+$/i.test(id);

export default function isMongoId(id: string) {
	return isHexdecimal(id) && id.length === 24;
}