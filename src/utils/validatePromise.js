/**
 * @flow
 */

import joi from 'joi';

export function validatePromise(schema, object: any): Promise<{ error: any }> {
	return new Promise((resolve) => {
		joi.validate(object, schema, (error, value) => {
			return resolve({ error });
		});
	});
}