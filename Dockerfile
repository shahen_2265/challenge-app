FROM node:latest

ARG workdir=/var/app

RUN mkdir -p ${workdir}
ADD . ${workdir}
WORKDIR ${workdir}
RUN npm i -g nodemon
RUN npm i
EXPOSE 3000
EXPOSE 9229
CMD ["npm","run","watch"]