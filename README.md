## API

```
POST -> /api/v1/auth/signin
{ username: string, password: string }

POST -> /api/v1/auth/signup
{ username: string, name: string, password: string }

POST -> /api/v1/quizes
{ questions: [{ description: string, answers: [{ text: string, isCorrect: boolean }] }] }

GET -> /api/v1/quizes

```